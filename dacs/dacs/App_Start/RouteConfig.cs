﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace dacs
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                name: "Trang chu",
                url: "",
                defaults: new { controller = "UserUI", action = "TrangChu", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Dang nhap",
                url: "dang-nhap-tai-khoan",
                defaults: new { controller = "UserPost", action = "Login" }
            );
            routes.MapRoute(
                name: "Dang ki",
                url: "dang-ky-tai-khoan",
                defaults: new { controller = "UserPost", action = "Register" }
            );
            routes.MapRoute(
                name: "Dang bai moi",
                url: "dang-tin-moi",
                defaults: new { controller = "UserPost", action = "Them" }
            );
            routes.MapRoute(
                name: "Bai Dang",
                url: "Bai-dang/{metatitle}-{id}",
                defaults: new { controller = "UserUI", action = "PostInfo", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            
        }
    }
}
