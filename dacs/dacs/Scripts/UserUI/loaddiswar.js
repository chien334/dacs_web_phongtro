﻿var user={
	init:function(){
		user.loadProvince();
		user.registerEvent();
	},
	registerEvent:function(){
		$('#search_city').off('change').on('change',function(){
			var id= $(this).val();
			if(id!=''){
				user.loadDistrict(id);
			}
			else{
				$('#district').html('');
			}
		});
		$('#district').off('change').on('change', function () {
		    var id = $(this).val();
		    if (id != '') {
		        user.loadWard(id);
		    }
		    else {
		        $('#ward').html('');
		    }
		});
	},
	loadProvince:function(){
		var html='';
		$.ajax({
		    url: '/UserUI/LoadProvinces',
			type:"POST",
			dataType:"json",
			success:function(data){
				var html='<option value="0">-- Chọn tỉnh/thành --</option>';
				$.each(data.data,function(i,item){
				    html += '<option value="' + item.PC_ID + '">' + item.PC_NAME + '</option>'
				});
				$('#search_city').html(html);
			}
		})
	},
	loadDistrict:function(id){
	    var html='';
	    $.ajax({
	        url: '/UserUI/LoadDistrict',
	        type:"POST",
	        data:{id:id},
	        dataType:"json",
	        success:function(data){
	            var html='<option value="0">--Chọn quận/huyện--</option>';
	            $.each(data.data,function(i,item){
	                html += '<option value="' + item.DIST_ID + '">' + item.DIST_NAME + '</option>'
	            });
	            $('#district').html(html);
	        }
	    })
	},
    loadWard:function(id){
        var html='';
        $.ajax({
            url: '/UserUI/LoadWard',
            type:"POST",
            data:{id:id},
            dataType:"json",
            success:function(data){
                var html='<option value="0">--Chọn phường/xã--</option>';
                $.each(data.data,function(i,item){
                    html += '<option value="' + item.WARD_ID + '">' + item.WARD_NAME + '</option>'
                });
                $('#ward').html(html);
            }
        })
    }
}
user.init();