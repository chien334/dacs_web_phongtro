﻿
function Layhinh(id) {
    var origin=window.location.origin;
    $.ajax({
        url: 'https://localhost:5001/api/imageplace/getImageByPlaceId/'+id,
        type: "GET",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                var slides = "";
                var control="";
                $.each(data, function (i, item) {
                    slides += '<li id="'+(i+1)+'"><img class="item_image" src="'+origin+'/Images/'+item.content+'" draggable="false"></li>';
                    control += '<li><a href="#" class="flex-active" id="num-' + (i + 1) + '" onclick="return thay(' + (i + 1) + ')">' + (i + 1) + '</a></li>';
                });
                $('#list-img').html(slides);
                $('#flex-control-nav').html(control);
            }
            else {
            }
        },
        eroor: function () {

        }
    });
}