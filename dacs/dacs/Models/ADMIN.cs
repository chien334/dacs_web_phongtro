namespace dacs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ADMIN")]
    public partial class ADMIN
    {
        [Key]
        public int ADMIN_ID { get; set; }

        [StringLength(32)]
        public string ADMIN_LOGIN_NAME { get; set; }

        [StringLength(64)]
        public string ADMIN_PASSWORD { get; set; }

        [StringLength(50)]
        public string ADMIN_FULL_NAME { get; set; }

        [StringLength(14)]
        public string ADMIN_PHONE { get; set; }

        [StringLength(100)]
        public string ADMIN_EMAIL { get; set; }

        [StringLength(300)]
        public string ADMIN_IMAGE { get; set; }

        public bool? ADMIN_STATUS { get; set; }
    }
}
