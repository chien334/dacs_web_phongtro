namespace dacs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PROVINCE_CITY
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PROVINCE_CITY()
        {
            DISTRICTs = new HashSet<DISTRICT>();
        }

        [Key]
        [StringLength(2)]
        public string PC_ID { get; set; }

        [StringLength(60)]
        public string PC_NAME { get; set; }

        [StringLength(60)]
        public string PC_TYPE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DISTRICT> DISTRICTs { get; set; }
    }
}
