namespace dacs.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class dbphongtromodels : DbContext
    {
        public dbphongtromodels()
            : base("name=dbphongtromodels")
        {
        }

        public virtual DbSet<ADMIN> ADMINs { get; set; }
        public virtual DbSet<DISTRICT> DISTRICTs { get; set; }
        public virtual DbSet<POST> POSTs { get; set; }
        public virtual DbSet<Post_Image> Post_Image { get; set; }
        public virtual DbSet<Post_Type> Post_Type { get; set; }
        public virtual DbSet<PROVINCE_CITY> PROVINCE_CITY { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<WARD> WARDs { get; set; }
        public virtual DbSet<Web_infomation> Web_infomation { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DISTRICT>()
                .Property(e => e.DIST_ID)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<DISTRICT>()
                .Property(e => e.PC_ID)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<POST>()
                .Property(e => e.WARD_ID)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PROVINCE_CITY>()
                .Property(e => e.PC_ID)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<WARD>()
                .Property(e => e.WARD_ID)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<WARD>()
                .Property(e => e.DIST_ID)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Web_infomation>()
                .Property(e => e.Phone)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Web_infomation>()
                .Property(e => e.Email)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Web_infomation>()
                .Property(e => e.Facebook)
                .IsFixedLength()
                .IsUnicode(false);
        }
    }
}
