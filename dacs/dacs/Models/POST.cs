namespace dacs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("POST")]
    public partial class POST
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public POST()
        {
            Post_Image = new HashSet<Post_Image>();
        }

        [Key]
        public long POST_ID { get; set; }

        [StringLength(200)]
        public string POST_TITLE { get; set; }

        [Column(TypeName = "ntext")]
        public string POST_CONTENT { get; set; }

        public int? POST_VIEW_TIMES { get; set; }

        public bool? POST_STATUS { get; set; }

        public DateTime? posted_date { get; set; }

        [StringLength(5)]
        public string WARD_ID { get; set; }

        [StringLength(200)]
        public string PLACE_ADDRESS { get; set; }

        public long? P_TypeID { get; set; }

        public double? price { get; set; }
        public double? PL_size { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Post_Image> Post_Image { get; set; }

        public virtual Post_Type Post_Type { get; set; }

        public virtual WARD WARD { get; set; }
    }
}
