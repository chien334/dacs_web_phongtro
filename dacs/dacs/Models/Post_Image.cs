namespace dacs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Post_Image
    {
        [Key]
        public long P_ImageID { get; set; }

        public long? POST_ID { get; set; }

        public byte[] P_ImageContent { get; set; }

        public int? P_ImageIndex { get; set; }

        public virtual POST POST { get; set; }
    }
}
