namespace dacs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Web_infomation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IF_ID { get; set; }

        [StringLength(200)]
        public string Logo_image { get; set; }

        [StringLength(200)]
        public string Address { get; set; }

        [StringLength(13)]
        public string Phone { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        [StringLength(100)]
        public string Facebook { get; set; }

        [StringLength(300)]
        public string footer { get; set; }
    }
}
