namespace dacs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("WARD")]
    public partial class WARD
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public WARD()
        {
            POSTs = new HashSet<POST>();
        }

        [Key]
        [StringLength(5)]
        public string WARD_ID { get; set; }

        [StringLength(3)]
        public string DIST_ID { get; set; }

        [StringLength(100)]
        public string WARD_NAME { get; set; }

        [StringLength(30)]
        public string WARD_TYPE { get; set; }

        public virtual DISTRICT DISTRICT { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<POST> POSTs { get; set; }
    }
}
