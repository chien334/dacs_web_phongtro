namespace dacs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DISTRICT")]
    public partial class DISTRICT
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DISTRICT()
        {
            WARDs = new HashSet<WARD>();
        }

        [Key]
        [StringLength(3)]
        public string DIST_ID { get; set; }

        [StringLength(2)]
        public string PC_ID { get; set; }

        [StringLength(100)]
        public string DIST_NAME { get; set; }

        [StringLength(20)]
        public string DIST_TYPE { get; set; }

        public virtual PROVINCE_CITY PROVINCE_CITY { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WARD> WARDs { get; set; }
    }
}
