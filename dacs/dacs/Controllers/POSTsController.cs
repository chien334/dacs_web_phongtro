﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using dacs.Models;

namespace dacs.Controllers
{
    public class POSTsController : Controller
    {
        private dbphongtromodels db = new dbphongtromodels();

        // GET: POSTs
        public ActionResult Index()
        {
            var pOSTs = db.POSTs.Include(p => p.Post_Type).Include(p => p.WARD);
            return View(pOSTs.ToList());
        }
        public PartialViewResult partioViewIndex()
        {
            var post_Image = db.Post_Image.Include(p => p.POST);
            return PartialView("partioviewIndex", post_Image.ToList());
        }

        // GET: POSTs/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            POST pOST = db.POSTs.Find(id);
            if (pOST == null)
            {
                return HttpNotFound();
            }
            return View(pOST);
        }


        public ActionResult Create()
        {
            ViewBag.P_TypeID = new SelectList(db.Post_Type, "P_TypeID", "P_TypeName");
            ViewBag.Province = new SelectList("", "PC_ID", "PC_NAME");
            ViewBag.Districts = new SelectList("", "DIST_ID", "DIST_NAME");
            ViewBag.WARD_ID = new SelectList("", "WARD_ID", "WARD_NAME");
            return View();
        }

        [HttpPost]
        public JsonResult LoadDistrict(string id)
        {
            // Parse the response body.
            return Json(db.DISTRICTs.Select(x => new
            {
                x.DIST_ID,
                x.DIST_NAME,
                x.PC_ID
            }).Where(a => a.PC_ID == id), JsonRequestBehavior.AllowGet);

        }
        public JsonResult LoadProvinces()
        {
            // Parse the response body.
            return Json(db.PROVINCE_CITY.Select(x => new
            {
                x.PC_ID,
                x.PC_NAME
            }), JsonRequestBehavior.AllowGet);

        }
        public JsonResult LoadWard(string id)
        {
            // Parse the response body.
            return Json(db.WARDs.Select(x => new
            {
                x.WARD_ID,
                x.WARD_NAME,
                x.DIST_ID
            }).Where(a => a.DIST_ID == id), JsonRequestBehavior.AllowGet);

        }

        // POST: POSTs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "POST_ID,POST_TITLE,POST_CONTENT,POST_VIEW_TIMES,POST_STATUS,posted_date,WARD_ID,PLACE_ADDRESS,P_TypeID,price,PL_size")] POST pOST)
        {
            if (ModelState.IsValid)
            {
                db.POSTs.Add(pOST);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.P_TypeID = new SelectList(db.Post_Type, "P_TypeID", "P_TypeName", pOST.P_TypeID);
            ViewBag.WARD_ID = new SelectList(db.WARDs, "WARD_ID", "WARD_NAME", pOST.WARD_ID);
            return View(pOST);
        }

        // GET: POSTs/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            POST pOST = db.POSTs.Find(id);
            if (pOST == null)
            {
                return HttpNotFound();
            }
            ViewBag.P_TypeID = new SelectList(db.Post_Type, "P_TypeID", "P_TypeName", pOST.P_TypeID);
            ViewBag.WARD_ID = new SelectList(db.WARDs, "WARD_ID", "WARD_ID", pOST.WARD_ID);
            return View(pOST);
        }

        // POST: POSTs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "POST_ID,POST_TITLE,POST_CONTENT,POST_VIEW_TIMES,POST_STATUS,posted_date,WARD_ID,PLACE_ADDRESS,P_TypeID,price,PL_size")] POST pOST)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pOST).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.P_TypeID = new SelectList(db.Post_Type, "P_TypeID", "P_TypeName", pOST.P_TypeID);
            ViewBag.WARD_ID = new SelectList(db.WARDs, "WARD_ID", "DIST_ID", pOST.WARD_ID);
            return View(pOST);
        }

        // GET: POSTs/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            POST pOST = db.POSTs.Find(id);
            if (pOST == null)
            {
                return HttpNotFound();
            }
            return View(pOST);
        }

        // POST: POSTs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            POST pOST = db.POSTs.Find(id);
            db.POSTs.Remove(pOST);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
