﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using dacs.Models;

namespace dacs.Controllers
{
    public class Post_TypeController : Controller
    {
        private dbphongtromodels db = new dbphongtromodels();

        // GET: Post_Type
        public ActionResult Index()
        {
            return View(db.Post_Type.ToList());
        }

        // GET: Post_Type/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post_Type post_Type = db.Post_Type.Find(id);
            if (post_Type == null)
            {
                return HttpNotFound();
            }
            return View(post_Type);
        }

        // GET: Post_Type/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Post_Type/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "P_TypeID,P_TypeName,P_Desciption")] Post_Type post_Type)
        {
            if (ModelState.IsValid)
            {
                db.Post_Type.Add(post_Type);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(post_Type);
        }

        // GET: Post_Type/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post_Type post_Type = db.Post_Type.Find(id);
            if (post_Type == null)
            {
                return HttpNotFound();
            }
            return View(post_Type);
        }

        // POST: Post_Type/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "P_TypeID,P_TypeName,P_Desciption")] Post_Type post_Type)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post_Type).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(post_Type);
        }

        // GET: Post_Type/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post_Type post_Type = db.Post_Type.Find(id);
            if (post_Type == null)
            {
                return HttpNotFound();
            }
            return View(post_Type);
        }

        // POST: Post_Type/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Post_Type post_Type = db.Post_Type.Find(id);
            db.Post_Type.Remove(post_Type);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
