﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using dacs.Models;
using PagedList;
using PagedList.Mvc;


namespace dacs.Controllers
{
    public class UserUIController : Controller
    {
        // GET: UserUI
        private dbphongtromodels db = new dbphongtromodels();
        public ActionResult TrangChu(int? page)
        {
            var pOSTs = db.POSTs.Include(p => p.WARD).Include(p=>p.WARD.DISTRICT).Include(p=>p.Post_Image);
            var data=pOSTs.ToList();
            for(int i=0;i<data.Count;i++)
            {
                data[i].Post_Image=data[i].Post_Image.ToList();
            }
            int pageSize = 8;
            int pageNum = (page ?? 1);
            return View(data.ToPagedList(pageNum, pageSize));
        }
        [HttpPost]
        public ActionResult FindPost(FormCollection form, int? page)
        {
            int pageSize = 8;
            int pageNum = (page ?? 1);
            string duong = form["duong"].ToString();
            string quan = form["quan"].ToString();
            string tinh = form["tinh"].ToString();
            ViewBag.quan = quan;
            ViewBag.tinh = tinh;
            ViewBag.duong = duong;
            if (quan == "0" && tinh == "0")
            {
                var pOSTs = db.POSTs.Include(p => p.WARD).Include(p => p.WARD.DISTRICT).Include(p => p.Post_Image).Where(p => p.PLACE_ADDRESS.Contains(duong));
                var data = pOSTs.ToList();
                for (int i = 0; i < data.Count; i++)
                {
                    data[i].Post_Image = data[i].Post_Image.ToList();
                }
                return View(data.ToPagedList(pageNum, pageSize));
            }
            else
            {
                if (int.Parse(quan) > 0)
                {
                    var pOSTs = db.POSTs.Include(p => p.WARD).Include(p => p.WARD.DISTRICT).Include(p => p.Post_Image).Where(p => p.PLACE_ADDRESS.Contains(duong) && p.WARD.DIST_ID == quan);
                    var data = pOSTs.ToList();
                    for (int i = 0; i < data.Count; i++)
                    {
                        data[i].Post_Image = data[i].Post_Image.ToList();
                    }
                    return View(data.ToPagedList(pageNum, pageSize));
                }
                else if (int.Parse(tinh) > 0)
                {
                    var pOSTs = db.POSTs.Include(p => p.WARD).Include(p => p.WARD.DISTRICT).Include(p => p.Post_Image).Where(p => p.PLACE_ADDRESS.Contains(duong) && p.WARD.DISTRICT.PC_ID == tinh);
                    var data = pOSTs.ToList();
                    for (int i = 0; i < data.Count; i++)
                    {
                        data[i].Post_Image = data[i].Post_Image.ToList();
                    }
                    return View(data.ToPagedList(pageNum, pageSize));
                }
            }
            var post = db.POSTs.Include(p => p.WARD).Include(p => p.WARD.DISTRICT).Include(p => p.Post_Image);
            var Data = post.ToList();
            for (int i = 0; i < Data.Count; i++)
            {
                Data[i].Post_Image = Data[i].Post_Image.ToList();
            }
            return View(Data.ToPagedList(pageNum, pageSize));
        }

        public ActionResult PostInfo(long id)
        {
            var data = db.POSTs.Include(p => p.WARD).Include(p => p.WARD.DISTRICT).Include(p => p.Post_Image).Where(d => d.POST_ID == id).FirstOrDefault();
            data.Post_Image = data.Post_Image.ToList();
            return View(data);
        }
        //Api load dữ liệu quận huyện theo id thành phố
        public async System.Threading.Tasks.Task<JsonResult> LoadDistrict(string id)
        {
            // trả về đối tượng json data: lấy dữ liệu quận huyện từ database có pc_id = id
            return Json(new
            {
                data = db.DISTRICTs.Select(x => new
                {
                    PC_ID = x.PC_ID,
                    DIST_ID = x.DIST_ID,
                    DIST_NAME = x.DIST_NAME
                }
                ).Where(d => d.PC_ID == id)
            }, JsonRequestBehavior.AllowGet);
        }
        //APi load dữ liệu tỉnh thành phố
        public async System.Threading.Tasks.Task<JsonResult> LoadProvinces()
        {
            // trả về đối tượng json data: lấy dữ liệu tỉnh thành phố từ database
            return Json(new
            {
                data = db.PROVINCE_CITY.Select(x => new
                {
                    PC_ID = x.PC_ID,
                    PC_NAME = x.PC_NAME
                }
            )
            }, JsonRequestBehavior.AllowGet);

        }
        //Api load dữ liệu phường xã theo id quận huyện
        public async System.Threading.Tasks.Task<JsonResult> LoadWard(string id)
        {
            // trả về đối tượng json data: lấy dữ liệu phường xã từ database có dist_id = id
            return Json(new
            {
                data = db.WARDs.Select(x => new
                {
                    WARD_ID = x.WARD_ID,
                    WARD_NAME = x.WARD_NAME,
                    DIST_ID = x.DIST_ID
                }
                ).Where(d => d.DIST_ID == id)
            }, JsonRequestBehavior.AllowGet);
        }
    }
}