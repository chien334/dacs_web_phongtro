﻿using dacs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace dacs.Controllers
{
    public class UserPostController : Controller
    {
        // GET: UserPost
        private dbphongtromodels db = new dbphongtromodels();
        public ActionResult Login()
        {
            if (Session["User"] != null)
                return RedirectToAction("TrangChu", "UserUI");
            return View();
        }
        [HttpPost]
        public ActionResult Login(string username, string password)
        {
            //duyệt csdl nếu có thì trả về 1 dòng dữ liệu, không thì trả về null
            var data = db.ADMINs.Where(d => d.ADMIN_LOGIN_NAME == username && d.ADMIN_PASSWORD == password).FirstOrDefault();
            //kiểm tra nếu ko có dữ liệu thì trả về trang login
            //nếu tìm thấy thì lưu biến session và trả về trang....
            if (data != null)
            {
                Session["user"] = data;
                if (data.ADMIN_STATUS == true)
                {
                    return RedirectToAction("Index", "ADMINs");
                }
                else
                {
                    return RedirectToAction("TrangChu", "UserUI");
                }            
            }
            ViewBag.error = "Tên đăng nhập hoặc mật khẩu không đúng";
            return View();
        }
        public ActionResult Logout()
        {
            if (Session["User"] != null)
            {
                Session["User"] = null;
            }
            return RedirectToAction("TrangChu", "UserUI");
        }
        public ActionResult Them()
        {
            if (Session["User"] == null)
                return RedirectToAction("Login", "UserPost");
            ViewBag.P_TypeID = db.Post_Type.Select(p=>p);
            return View();
        }
        [HttpPost]
        public ActionResult Them(string POST_TITLE,string POST_CONTENT,string ward,string PLACE_ADDRESS,string loai,string price,string PL_size)
        {  
            POST p = new Models.POST();
            p.POST_TITLE = POST_TITLE;
            p.POST_CONTENT = POST_CONTENT;
            p.posted_date = DateTime.Now;
            p.POST_STATUS = true;
            p.POST_VIEW_TIMES = 0;
            p.WARD_ID = ward;
            p.PLACE_ADDRESS = PLACE_ADDRESS;
            p.P_TypeID = long.Parse(loai);
            p.price = double.Parse(price);
            p.PL_size = double.Parse(PL_size);
            db.POSTs.Add(p);
            db.SaveChanges();
            return RedirectToAction("TrangChu","UserUI");
        }
        //Đăng kí
        public ActionResult Register()
        {
            if (Session["User"] != null)
            {
                return RedirectToAction("Trangchu", "UserUI");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Register(string username, string password,string Displayname, string email, string image,string phone)
        {
            //duyệt csdl nếu có thì trả về 1 dòng dữ liệu, không thì trả về null
            var data = db.ADMINs.Where(d => d.ADMIN_EMAIL == email);
            if (data.Count() > 0)
            {
                ViewBag.error = "Email đã tồn tại!";
                return View();
            }
            ADMIN p = new Models.ADMIN();
            p.ADMIN_LOGIN_NAME = username;
            p.ADMIN_PASSWORD = password;
            p.ADMIN_FULL_NAME = Displayname;
            p.ADMIN_IMAGE = image;
            p.ADMIN_PHONE = phone;
            p.ADMIN_EMAIL = email;
            p.ADMIN_STATUS = false;
            db.ADMINs.Add(p);
            db.SaveChanges();
            Session["user"] = p;
            return RedirectToAction("TrangChu", "UserUI");
        }
        public async System.Threading.Tasks.Task<int> checkmail(string email)
        {
            // trả về đối tượng json data: lấy dữ liệu phường xã từ database có dist_id = id
            var data = db.ADMINs.Where(d => d.ADMIN_EMAIL == email);
            if (data.Count() > 0)
            {
                return data.Count();
            }
            return 0;
        }
    }
}