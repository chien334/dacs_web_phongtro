﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using dacs.Models;

namespace dacs.Controllers
{
    public class WARDsController : Controller
    {
        private dbphongtromodels db = new dbphongtromodels();

        // GET: WARDs
        public ActionResult Index()
        {
            var wARDs = db.WARDs.Include(w => w.DISTRICT);
            return View(wARDs.ToList());
        }

        // GET: WARDs/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WARD wARD = db.WARDs.Find(id);
            if (wARD == null)
            {
                return HttpNotFound();
            }
            return View(wARD);
        }

        // GET: WARDs/Create
        public ActionResult Create()
        {
            ViewBag.DIST_ID = new SelectList(db.DISTRICTs, "DIST_ID", "PC_ID");
            return View();
        }

        // POST: WARDs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "WARD_ID,DIST_ID,WARD_NAME,WARD_TYPE")] WARD wARD)
        {
            if (ModelState.IsValid)
            {
                db.WARDs.Add(wARD);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DIST_ID = new SelectList(db.DISTRICTs, "DIST_ID", "PC_ID", wARD.DIST_ID);
            return View(wARD);
        }

        // GET: WARDs/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WARD wARD = db.WARDs.Find(id);
            if (wARD == null)
            {
                return HttpNotFound();
            }
            ViewBag.DIST_ID = new SelectList(db.DISTRICTs, "DIST_ID", "PC_ID", wARD.DIST_ID);
            return View(wARD);
        }

        // POST: WARDs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "WARD_ID,DIST_ID,WARD_NAME,WARD_TYPE")] WARD wARD)
        {
            if (ModelState.IsValid)
            {
                db.Entry(wARD).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DIST_ID = new SelectList(db.DISTRICTs, "DIST_ID", "PC_ID", wARD.DIST_ID);
            return View(wARD);
        }

        // GET: WARDs/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WARD wARD = db.WARDs.Find(id);
            if (wARD == null)
            {
                return HttpNotFound();
            }
            return View(wARD);
        }

        // POST: WARDs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            WARD wARD = db.WARDs.Find(id);
            db.WARDs.Remove(wARD);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
