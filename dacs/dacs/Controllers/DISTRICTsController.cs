﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using dacs.Models;

namespace dacs.Controllers
{
    public class DISTRICTsController : Controller
    {
        private dbphongtromodels db = new dbphongtromodels();

        // GET: DISTRICTs
        public ActionResult Index()
        {
            var dISTRICTs = db.DISTRICTs.Include(d => d.PROVINCE_CITY);
            return View(dISTRICTs.ToList());
        }

        // GET: DISTRICTs/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DISTRICT dISTRICT = db.DISTRICTs.Find(id);
            if (dISTRICT == null)
            {
                return HttpNotFound();
            }
            return View(dISTRICT);
        }

        // GET: DISTRICTs/Create
        public ActionResult Create()
        {
            ViewBag.PC_ID = new SelectList(db.PROVINCE_CITY, "PC_ID", "PC_NAME");
            return View();
        }

        // POST: DISTRICTs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DIST_ID,PC_ID,DIST_NAME,DIST_TYPE")] DISTRICT dISTRICT)
        {
            if (ModelState.IsValid)
            {
                db.DISTRICTs.Add(dISTRICT);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PC_ID = new SelectList(db.PROVINCE_CITY, "PC_ID", "PC_NAME", dISTRICT.PC_ID);
            return View(dISTRICT);
        }

        // GET: DISTRICTs/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DISTRICT dISTRICT = db.DISTRICTs.Find(id);
            if (dISTRICT == null)
            {
                return HttpNotFound();
            }
            ViewBag.PC_ID = new SelectList(db.PROVINCE_CITY, "PC_ID", "PC_NAME", dISTRICT.PC_ID);
            return View(dISTRICT);
        }

        // POST: DISTRICTs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DIST_ID,PC_ID,DIST_NAME,DIST_TYPE")] DISTRICT dISTRICT)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dISTRICT).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PC_ID = new SelectList(db.PROVINCE_CITY, "PC_ID", "PC_NAME", dISTRICT.PC_ID);
            return View(dISTRICT);
        }

        // GET: DISTRICTs/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DISTRICT dISTRICT = db.DISTRICTs.Find(id);
            if (dISTRICT == null)
            {
                return HttpNotFound();
            }
            return View(dISTRICT);
        }

        // POST: DISTRICTs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            DISTRICT dISTRICT = db.DISTRICTs.Find(id);
            db.DISTRICTs.Remove(dISTRICT);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
