﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using dacs.Models;

namespace dacs.Controllers
{
    public class ADMINsController : Controller
    {
        private dbphongtromodels db = new dbphongtromodels();
        public ActionResult Login()
        {
            //Chuyển về trang chính
            /*if (Session["User"] != null)
                return RedirectToAction("Index", "User");*/
            return View();
        }
        [HttpPost]
        public ActionResult Login(FormCollection form)
        {
            // lấy username và password từ from
            string name = form["uname"].ToString();
            string pass = form["psw"].ToString();
            //duyệt csdl nếu có thì trả về 1 dòng dữ liệu, không thì trả về null
            var data = db.ADMINs.Where(d => d.ADMIN_LOGIN_NAME == name && d.ADMIN_PASSWORD==pass).FirstOrDefault();
            //kiểm tra nếu ko có dữ liệu thì trả về trang login
            //nếu tìm thấy thì lưu biến session và trả về trang....
            if (data != null)
            {
                Session["user"] = data;
                return RedirectToAction("TrangChu", "UserUI");
            }
            return View();
        }
        // GET: ADMINs
        public ActionResult Index()
        {
            return View(db.ADMINs.ToList());
        }

        // GET: ADMINs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ADMIN aDMIN = db.ADMINs.Find(id);
            if (aDMIN == null)
            {
                return HttpNotFound();
            }
            return View(aDMIN);
        }

        // GET: ADMINs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ADMINs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ADMIN_ID,ADMIN_LOGIN_NAME,ADMIN_PASSWORD,ADMIN_FULL_NAME,ADMIN_PHONE,ADMIN_EMAIL,ADMIN_IMAGE,ADMIN_STATUS")] ADMIN aDMIN)
        {
            if (ModelState.IsValid)
            {
                db.ADMINs.Add(aDMIN);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(aDMIN);
        }

        // GET: ADMINs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ADMIN aDMIN = db.ADMINs.Find(id);
            if (aDMIN == null)
            {
                return HttpNotFound();
            }
            return View(aDMIN);
        }

        // POST: ADMINs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ADMIN_ID,ADMIN_LOGIN_NAME,ADMIN_PASSWORD,ADMIN_FULL_NAME,ADMIN_PHONE,ADMIN_EMAIL,ADMIN_IMAGE,ADMIN_STATUS")] ADMIN aDMIN)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aDMIN).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(aDMIN);
        }

        // GET: ADMINs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ADMIN aDMIN = db.ADMINs.Find(id);
            if (aDMIN == null)
            {
                return HttpNotFound();
            }
            return View(aDMIN);
        }

        // POST: ADMINs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ADMIN aDMIN = db.ADMINs.Find(id);
            db.ADMINs.Remove(aDMIN);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
