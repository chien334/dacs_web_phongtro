﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using dacs.Models;

namespace dacs.Controllers
{
    public class Post_ImageController : Controller
    {
        private dbphongtromodels db = new dbphongtromodels();

        // GET: Post_Image
        public ActionResult Index()
        {
            var post_Image = db.Post_Image.Include(p => p.POST);
            return PartialView(post_Image.ToList());
        }

        // GET: Post_Image/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post_Image post_Image = db.Post_Image.Find(id);
            if (post_Image == null)
            {
                return HttpNotFound();
            }
            return PartialView(post_Image);
        }

        // GET: Post_Image/Create
        public ActionResult Create()
        {
            //ViewBag.POST_ID = new SelectList(db.POSTs, "POST_ID", "POST_TITLE");    
            return PartialView();
        }

        [HttpPost]
        public ActionResult Create(IEnumerable<HttpPostedFileBase> files)
        {
            Post_Image image;
            int dem = 0;
            foreach (var file in files)
            {
                image = new Post_Image();

                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream ms = new MemoryStream();
                    file.InputStream.CopyTo(ms);
                    byte[] img = ms.ToArray();
                    //image.POST_ID = id;
                    image.P_ImageContent = img;
                    image.P_ImageIndex = dem;
                    db.Post_Image.Add(image);
                    db.SaveChanges();
                }
                dem++;
            }
            return PartialView();
        }
        // GET: Post_Image/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post_Image post_Image = db.Post_Image.Find(id);
            if (post_Image == null)
            {
                return HttpNotFound();
            }
            ViewBag.POST_ID = new SelectList(db.POSTs, "POST_ID", "POST_TITLE", post_Image.POST_ID);
            return View(post_Image);
        }

        // POST: Post_Image/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "P_ImageID,POST_ID,P_ImageContent,P_ImageIndex")] Post_Image post_Image)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post_Image).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.POST_ID = new SelectList(db.POSTs, "POST_ID", "POST_TITLE", post_Image.POST_ID);
            return View(post_Image);
        }

        // GET: Post_Image/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post_Image post_Image = db.Post_Image.Find(id);
            if (post_Image == null)
            {
                return HttpNotFound();
            }
            return View(post_Image);
        }

        // POST: Post_Image/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Post_Image post_Image = db.Post_Image.Find(id);
            db.Post_Image.Remove(post_Image);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
