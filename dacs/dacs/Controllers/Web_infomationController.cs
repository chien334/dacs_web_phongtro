﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using dacs.Models;

namespace dacs.Controllers
{
    public class Web_infomationController : Controller
    {
        private dbphongtromodels db = new dbphongtromodels();

        // GET: Web_infomation
        public ActionResult Index()
        {
            return View(db.Web_infomation.ToList());
        }

        // GET: Web_infomation/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Web_infomation web_infomation = db.Web_infomation.Find(id);
            if (web_infomation == null)
            {
                return HttpNotFound();
            }
            return View(web_infomation);
        }

        // GET: Web_infomation/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Web_infomation/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IF_ID,Logo_image,Address,Phone,Email,Facebook,footer")] Web_infomation web_infomation)
        {
            if (ModelState.IsValid)
            {
                db.Web_infomation.Add(web_infomation);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(web_infomation);
        }

        // GET: Web_infomation/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Web_infomation web_infomation = db.Web_infomation.Find(id);
            if (web_infomation == null)
            {
                return HttpNotFound();
            }
            return View(web_infomation);
        }

        // POST: Web_infomation/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IF_ID,Logo_image,Address,Phone,Email,Facebook,footer")] Web_infomation web_infomation)
        {
            if (ModelState.IsValid)
            {
                db.Entry(web_infomation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(web_infomation);
        }

        // GET: Web_infomation/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Web_infomation web_infomation = db.Web_infomation.Find(id);
            if (web_infomation == null)
            {
                return HttpNotFound();
            }
            return View(web_infomation);
        }

        // POST: Web_infomation/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Web_infomation web_infomation = db.Web_infomation.Find(id);
            db.Web_infomation.Remove(web_infomation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
