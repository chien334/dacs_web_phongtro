﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using dacs.Models;

namespace dacs.Controllers
{
    public class PROVINCE_CITYController : Controller
    {
        private dbphongtromodels db = new dbphongtromodels();

        // GET: PROVINCE_CITY
        public ActionResult Index()
        {
            return View(db.PROVINCE_CITY.ToList());
        }

        // GET: PROVINCE_CITY/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PROVINCE_CITY pROVINCE_CITY = db.PROVINCE_CITY.Find(id);
            if (pROVINCE_CITY == null)
            {
                return HttpNotFound();
            }
            return View(pROVINCE_CITY);
        }

        // GET: PROVINCE_CITY/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PROVINCE_CITY/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PC_ID,PC_NAME,PC_TYPE")] PROVINCE_CITY pROVINCE_CITY)
        {
            if (ModelState.IsValid)
            {
                db.PROVINCE_CITY.Add(pROVINCE_CITY);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pROVINCE_CITY);
        }

        // GET: PROVINCE_CITY/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PROVINCE_CITY pROVINCE_CITY = db.PROVINCE_CITY.Find(id);
            if (pROVINCE_CITY == null)
            {
                return HttpNotFound();
            }
            return View(pROVINCE_CITY);
        }

        // POST: PROVINCE_CITY/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PC_ID,PC_NAME,PC_TYPE")] PROVINCE_CITY pROVINCE_CITY)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pROVINCE_CITY).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pROVINCE_CITY);
        }

        // GET: PROVINCE_CITY/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PROVINCE_CITY pROVINCE_CITY = db.PROVINCE_CITY.Find(id);
            if (pROVINCE_CITY == null)
            {
                return HttpNotFound();
            }
            return View(pROVINCE_CITY);
        }

        // POST: PROVINCE_CITY/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            PROVINCE_CITY pROVINCE_CITY = db.PROVINCE_CITY.Find(id);
            db.PROVINCE_CITY.Remove(pROVINCE_CITY);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
